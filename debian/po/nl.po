# Dutch translation of miniupnpd debconf templates.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the miniupnpd package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# Frans Spiesschaert <Frans.Spiesschaert@yucom.be>, 2018, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: miniupnpd_2.1-3\n"
"Report-Msgid-Bugs-To: miniupnpd@packages.debian.org\n"
"POT-Creation-Date: 2019-01-06 21:56+0800\n"
"PO-Revision-Date: 2019-01-14 23:13+0100\n"
"Last-Translator: Frans Spiesschaert <Frans.Spiesschaert@yucom.be>\n"
"Language-Team: Debian Dutch l10n Team <debian-l10n-dutch@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Gtranslator 2.91.7\n"

#. Type: boolean
#. Description
#: ../miniupnpd.templates:2001
msgid "Start the MiniUPnP daemon automatically?"
msgstr "De MiniUPnP-achtergronddienst automatisch starten?"

#. Type: boolean
#. Description
#: ../miniupnpd.templates:2001
msgid ""
"Choose this option if the MiniUPnP daemon should start automatically, now "
"and at boot time."
msgstr ""
"Kies voor deze optie als de MiniUPnP-achtergronddienst automatisch moet "
"starten, nu en bij het opstarten van de computer."

#. Type: string
#. Description
#: ../miniupnpd.templates:3001
msgid "Interfaces to listen on for UPnP queries:"
msgstr "Interfaces waarop geluisterd moet worden naar UPnP-verzoeken:"

#. Type: string
#. Description
#: ../miniupnpd.templates:3001
msgid ""
"The MiniUPnP daemon will listen for requests on the local network. Please "
"enter the interfaces or IP addresses it should listen on, separated by space."
msgstr ""
"De MiniUPnP-achtergronddienst luistert op het lokale netwerk naar aanvragen. "
"Geef de interfaces of de IP-adressen op waarop de dienst moet luisteren, "
"gescheiden door witruimte."

#. Type: string
#. Description
#: ../miniupnpd.templates:3001
msgid ""
"Interface names are preferred, and required if you plan to enable IPv6 port "
"forwarding."
msgstr ""
"Interfacenamen genieten de voorkeur en zijn vereist als u IPv6 port "
"forwarding wilt activeren."

#. Type: string
#. Description
#: ../miniupnpd.templates:4001
msgid "External WAN network interface to open ports on:"
msgstr ""
"Netwerk-interface voor het externe WAN waarop poorten geopend moeten worden:"

#. Type: string
#. Description
#: ../miniupnpd.templates:4001
msgid ""
"The MiniUPnP daemon will listen on a specific IP address on the local "
"network, then open ports on the WAN interface. Please enter the name of the "
"WAN network interface on which the MiniUPnP daemon should perform port "
"forwarding."
msgstr ""
"De MiniUPnP-achtergronddienst zal op een specifiek IP-adres van het lokale "
"netwerk luisteren en dan op de WAN-interface poorten openen. Geef de naam op "
"van de WAN-netwerkinterface waarop de MiniUPnP-achtergronddienst port-"
"forwarding moet uitvoeren."

#. Type: boolean
#. Description
#: ../miniupnpd.templates:5001
msgid "Enable IPv6 firewall chain?"
msgstr "IPv6 firewall chain (firewallregels) activeren?"

#. Type: boolean
#. Description
#: ../miniupnpd.templates:5001
msgid ""
"Please specify whether the MiniUPnP daemon should run its ip6tables script "
"on startup to initialize the IPv6 firewall chain."
msgstr ""
"Geef aan of de MiniUPnP-achtergronddienst bij het starten zijn ip6tables-"
"script moet uitvoeren om de IPv6 firewall chain (firewallregels) te "
"initialiseren."

#. Type: boolean
#. Description
#: ../miniupnpd.templates:5001
msgid ""
"Note: This option is useless if you don't block any IPv6 forwarded traffic."
msgstr ""
"Merk op: deze optie is zinloos indien u geen doorgestuurde IPv6-trafiek "
"blokkeert."

#. Type: boolean
#. Description
#: ../miniupnpd.templates:6001
msgid "Force reporting IGDv1 in rootDesc?"
msgstr ""
"De achtergronddienst zich in rootDesc verplicht als IGDv1 laten voordoen?"

#. Type: boolean
#. Description
#: ../miniupnpd.templates:6001
msgid ""
"Some IGD clients (most notably Microsoft® Windows® BITS) look for IGDv1 and "
"do not recognize IGDv2 as a valid IGD service. This option will fool them by "
"pretending itself to be IGDv1."
msgstr ""
"Sommige IGD-clients (in het bijzonder Microsoft® Windows® BITS) zijn op zoek "
"naar IGDv1 en herkennen IGDv2 niet als een geldige IGD-dienst. Met deze "
"optie bedrieg je hen door de achtergronddienst zich te laten voordoen als "
"IGDv1."

#. Type: boolean
#. Description
#: ../miniupnpd.templates:6001
msgid ""
"Of course you will lose IGDv2 functions (notably IPv6 support) by enabling "
"this."
msgstr ""
"Natuurlijk zult u door dit in te schakelen IGDv2-functionaliteit (in het "
"bijzonder ondersteuning voor IPv6) verliezen."
